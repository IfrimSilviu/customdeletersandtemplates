#include "Any.h"
#include "Configuration.h"
#include "TestData.h"

#include <iostream>
int main()
{
	{
		Any a;
		TestData t;
		a = t;
	}
	//Any a;
	//double val = 2;
	//a = val;
	//val = 7;
	//a = val;
	//std::cout << "Test with int "
	//	<< ((a.get<double>() == val) ? "PASSED" : "FAILED") << '\n';
	return 0;
}