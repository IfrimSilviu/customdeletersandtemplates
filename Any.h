#pragma once


#include "Utilities.hpp"

#include <iostream>
#include <functional>
#include <stdexcept>

typedef void(*DeleterFunction)(void*);

template <typename T>
void templateDeleter(void* ptr)
{
	T* asT = static_cast<T*>(ptr);
	delete asT;
}

typedef void(*CopyFunction)(void*, void*);

template <typename T>
T copyFunction(void* dest, void* src)
{
	T* srcAsT = (T*)src;
	T* destAsT = new T(*srcAsT);
	dest = destAsT;
}


class Any
{
public:
	Any()
	{
		_ptr = nullptr;
	}

	Any(const Any& other);

	Any& operator = (const Any& other);

	template <typename T>
	Any(T value);

	template <typename T>
	Any& operator = (const T& other);

	template <typename T>
	const T& get() const;

	~Any();

private:

	template <typename T>
	Any& copy(const T& other);

	std::function<void(void*)> _deleter;
	CopyFunction _copier;

	void* _ptr;
};


template <typename T, typename U>
bool ItsAKindOf(U z)
{
	return dynamic_cast<T>(z) != nullptr;
}

inline Any::Any(const Any & other)
{
	other._copier(_ptr, other._ptr);
	_copier = other._copier;
	_deleter = other._deleter;
}

inline Any & Any::operator=(const Any & other)
{
	// TODO: insert return statement here
}

Any::~Any()
{
	if (_ptr)
	{
		std::cout << "DEleting Any.\n";
		_deleter(_ptr);
		
	}
}

template<typename T>
inline Any::Any(T value)
{
	_ptr = new T;
	*_ptr = value;
}

template<typename T>
inline Any & Any::operator=(const T & other)
{
	this->~Any();

	_deleter = [](void* p)
	{
		T* cast = static_cast<T*>(p);
		delete cast;
	};

	_deleter = templateDeleter<T>;
	_copier = copyFunction<T>;
	_ptr = new T(other);
	return *this;
}

template<typename T>
inline const T & Any::get() const
{
	//auto x = dynamic_cast<T*, void*>(_ptr);
	//if(x == nullptr)
	//	throw std::runtime_error("Type error.");

	//if (ItsAKindOf<T*>(_ptr) != false)
	//	throw std::runtime_error("Type error.");

	return (*static_cast<T*>(_ptr));
}

template<typename T>
inline Any & Any::copy(const T & other)
{
	
}
